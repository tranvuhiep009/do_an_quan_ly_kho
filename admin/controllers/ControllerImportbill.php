<?php 
	//inlude file model vao day
	include "models/ModelImportbill.php";
	class ControllerImportbill extends Controller{
		//ke thua class model
		use ModelImportbill;
		public function index(){
			//quy dinh so ban ghi tren mot trang
			$recordPerPage = 40;
			//tinh so trang
			$numPage = ceil($this->modelTotalRecord()/$recordPerPage);
			//lay du lieu tu model
			$data = $this->modelRead($recordPerPage);
			//goi view, truyen du lieu ra view
			$this->loadView("importbill/ViewImportbill.php",array("data"=>$data,"numPage"=>$numPage));
		}
		public function create(){
			//tao bien $action de biet duoc khi an nut submit se dan den dau
			$action = "index.php?controller=importbill&action=createPost";
			//goi view, truyen du lieu ra view
			$this->loadView("importbill/ViewFormImportbill.php",array("action"=>$action));
		}
		public function createPost(){
			//goi ham modelCreate de create ban ghi
			$this->modelCreate();
			//quay tro lai trang news
			header("location:index.php?controller=importbill");
		}
		public function delete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//goi ham modelUpdate de update ban ghi
			$this->modelDelete();
			//quay tro lai trang news
			header("location:index.php?controller=importbill");
		}
        public function detail(){
            //goi ham modelCreate de create ban ghi
            $data = $this->modelDetail();
            //quay tro lai trang news
            $this->loadView("importbill/ViewDetail.php",array("data"=>$data));
        }
        public function ycnhaphang(){
            $this->loadView("importbill/ViewRequest.php");
        }
        public function ycnhaphangncc(){
            $this->loadView("importbill/ViewRequestNcc.php");
        }
	}
 ?>