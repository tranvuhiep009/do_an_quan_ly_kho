<?php 
	include "models/ModelStatistical.php";
	class ControllerHome extends Controller{
		use ModelStatistical;
		//ham tao
		public function __construct(){
			//kiem tra xem user da dang nhap chua
			$this->authentication();
		}
		public function index(){
			//load view
			$slsp=$this->getsoluongsp();
			$tkpn=$this->getslphieunhap();
			$tkpx=$this->getslphieuxuat();
			$this->loadView("ViewHome.php",array("slsp"=>$slsp,"tkpn"=>$tkpn,"tkpx"=>$tkpx));
		}
		public function detailsp(){
			$data=$this->getsp();
			$this->loadView("thongkesp.php",array("data"=>$data));
		}
		public function detailpn(){
			$theothang=[];
			for ($i=1; $i < 13; $i++) { 
				$test=$this->getpntheothang($i)->slpntt;
				$theothang[]=$test;
			}
			// $theothang=$this->getpntheothang(6);
			// var_dump($theothang);exit();
			$this->loadView("thongkepn.php",array("theothang"=>$theothang));
		}
		public function detailpx(){
			$theothang=[];
			for ($i=1; $i < 13; $i++) { 
				$test=$this->getpxtheothang($i)->slpntx;
				$theothang[]=$test;
			}
			// $theothang=$this->getpntheothang(6);
			// var_dump($theothang);exit();
			$this->loadView("thongkepx.php",array("theothang"=>$theothang));
		}
	}
?>