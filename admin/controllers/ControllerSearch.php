<?php 
	include "models/ModelSearch.php";
	class ControllerSearch extends Controller{
		use ModelSearch;
		public function index(){
			$fromPrice = isset($_GET["fromPrice"]) ? $_GET["fromPrice"] : 0;
			$toPrice = isset($_GET["toPrice"]) ? $_GET["toPrice"] : 0;
			//quy dinh so ban ghi tren mot trang
			$recordPerPage = 4;
			//tinh so trang
			$numPage = ceil($this->modelTotalRecord()/$recordPerPage);
			//lay du lieu tu model
			$data = $this->modelRead($recordPerPage);
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearch.php",array("data"=>$data,"numPage"=>$numPage,"fromPrice"=>$fromPrice,"toPrice"=>$toPrice));
		}
		public function searchKey2(){
			$data = $this->modelRead2();
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearchKey.php",array("data"=>$data));
		}
		public function searchKey_ncc(){
			$data = $this->modelRead_ncc();
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearchNcc.php",array("data"=>$data));
		}
		public function searchKey_sp(){
			$data = $this->modelRead_sp();
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearchSp.php",array("data"=>$data));
		}
		public function searchkho(){
			$data = $this->modelReadKho();
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearchKho.php",array("data"=>$data));
		}
		public function searchKey_khach(){
			$data = $this->modelReadkhach();
			//goi view, truyen du lieu ra view
			$this->loadView("ViewSearch_khach.php",array("data"=>$data));
		}
	}
 ?>