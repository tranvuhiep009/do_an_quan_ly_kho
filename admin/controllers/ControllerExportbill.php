<?php 
	//inlude file model vao day
	include "models/ModelExportbill.php";
	class ControllerExportbill extends Controller{
		//ke thua class model
		use ModelExportbill;
		public function index(){
			//quy dinh so ban ghi tren mot trang
			$recordPerPage = 40;
			//tinh so trang
			$numPage = ceil($this->modelTotalRecord()/$recordPerPage);
			//lay du lieu tu model
			$data = $this->modelRead($recordPerPage);
			//goi view, truyen du lieu ra view
			$this->loadView("exportbill/ViewExportbill.php",array("data"=>$data,"numPage"=>$numPage));
		}
		public function create(){
			//tao bien $action de biet duoc khi an nut submit se dan den dau
			$action = "index.php?controller=exportbill&action=createPost";
			//goi view, truyen du lieu ra view
			$this->loadView("exportbill/ViewFormExportbill.php",array("action"=>$action));
		}
		public function createPost(){
			//goi ham modelCreate de create ban ghi
			$this->modelCreate();
			//quay tro lai trang news
			header("location:index.php?controller=exportbill");
		}
		public function delete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//goi ham modelUpdate de update ban ghi
			$this->modelDelete();
			//quay tro lai trang news
			header("location:index.php?controller=exportbill");
		}
        public function detail(){
            //goi ham modelCreate de create ban ghi
            $data = $this->modelDetail();
            //quay tro lai trang news
            $this->loadView("exportbill/ViewDetail.php",array("data"=>$data));
        }
        public function ycxuathang(){
            $this->loadView("exportbill/ViewRequest.php");
        }
        public function ycxuathangstore(){
        	//quy dinh so ban ghi tren mot trang
			$data = $this->showproduct();
			//goi view, truyen du lieu ra view
            $this->loadView("exportbill/ViewRequestStore.php",array("data"=>$data));
        }
	}
 ?>