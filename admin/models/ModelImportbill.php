<?php 
	trait ModelImportbill{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from importbill order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from importbill");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from importbill where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		
		public function modelCreate(){
			// var_dump($_POST);die();
			$id = isset($_POST["id"])&&$_POST["id"] > 0 ? $_POST["id"] : 0;
			$create_date = date('Y-m-t');
			$content = $_POST["content"];
			$warehouse_id = $_POST["warehouse_id"];
			$product_name = $_POST["product_name"];
			$quantity = $_POST["quantity"];
			// var_dump($quantity);die();
			$price = $_POST["price"];
			$user_name = $_SESSION["user_login"];
			// var_dump("content");die()
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert importbill set id=:var_id,create_date=:var_create_date,content=:var_content,user_name=:var_user_name,warehouse_id=:var_warehouse_id");
			$query->execute(array("var_id"=>$id,"var_create_date"=>$create_date,"var_content"=>$content,"var_user_name"=>$user_name,"var_warehouse_id"=>$warehouse_id));
            $query = $conn->prepare("insert importbilldetails set importbill_id=:var_importbill_id,product_name=:var_product_name,quantity=:var_quantity,price=:var_price");
            $query->execute(array("var_importbill_id"=>$id,"var_product_name"=>$product_name,"var_quantity"=>$quantity,"var_price"=>$price));
			//---
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from importbill where id=$id");
		}
		public function modelGetWarehouse($ware_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse where id=$ware_id");
			return $query->fetch();
		}
		public function modelListWarehouse(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse");
			return $query->fetchAll();
		}
        public function modelDetail(){
            $id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
            $conn = Connection::getInstance();
            $query = $conn->query("select * from importbill where id=$id ");
            return $query->fetch();
        }
        public function modelDetailbill(){
            $id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
            $conn = Connection::getInstance();
            $query = $conn->query("select * from importbilldetails where importbill_id=$id ");
            return $query->fetch();
        }
	}
 ?>