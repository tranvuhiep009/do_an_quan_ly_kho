<?php 
	trait ModelSearch{
		public function modelAjaxSearch(){
			$conn = Connection::getInstance();
			$key = isset($_GET["key"]) ? $_GET["key"] : "";
			$query = $conn->query("select id,name,photo from products where name like '%$key%'");
			return $query->fetchAll();
		}
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			$id=$_GET['id'];
			$fromPrice = isset($_GET["fromPrice"]) ? $_GET["fromPrice"] : 0;
			$toPrice = isset($_GET["toPrice"]) ? $_GET["toPrice"] : 0;
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products where price >= $fromPrice and price <= $toPrice and category_id=$id order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			$id=$_GET['id'];
			$fromPrice = isset($_GET["fromPrice"]) ? $_GET["fromPrice"] : 0;
			$toPrice = isset($_GET["toPrice"]) ? $_GET["toPrice"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from products where price >= $fromPrice and price <= $toPrice and category_id=$id");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		public function modelRead2(){
			$keysearch=isset($_GET['keysearch'])?$_GET['keysearch']:"";
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where user_name like '%$keysearch%'");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		public function modelRead_ncc(){
			$keysearch=isset($_GET['keysearch'])?$_GET['keysearch']:"";
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers where supplier_name like '%$keysearch%'");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		public function modelGetSupplier($Sup_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers where id=$Sup_id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelRead_sp(){
			$keysearch=isset($_GET['keysearch'])?$_GET['keysearch']:"";
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products where product_name like '%$keysearch%'");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		public function modelReadKho(){
			$keysearch=isset($_GET['keysearch'])?$_GET['keysearch']:"";
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse where warehouse_name like '%$keysearch%'");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		public function modelGetUser($user_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where id=$user_id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelReadkhach(){
			$keysearch=isset($_GET['keysearch'])?$_GET['keysearch']:"";
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from customers where customer_name like '%$keysearch%'");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
	}
 ?>