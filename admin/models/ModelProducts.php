<?php 
	trait ModelProducts{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from products");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelUpdate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$product_name = $_POST["product_name"];
			$color = $_POST["color"];
			$size = $_POST["size"];
			$quantity = $_POST["quantity"];
			$price = $_POST["price"];
			$supplier_id  = $_POST["supplier_id"];
			//update name
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("update products set product_name=:var_product_name,color=:var_color,size=:var_size,quantity=:var_quantity,price=:var_price,supplier_id=:var_supplier_id where id=$id");
			$query->execute(array("var_product_name"=>$product_name,"var_color"=>$color,"var_size"=>$size,"var_price"=>$price,"var_quantity"=>$quantity,"var_supplier_id"=>$supplier_id));	
			//---
		}
		public function modelCreate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$product_name = $_POST["product_name"];
			$color = $_POST["color"];
			$size = $_POST["size"];
			$quantity = $_POST["quantity"];
			$price = $_POST["price"];
			$supplier_id  = $_POST["supplier_id"];
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert products set product_name=:var_product_name,color=:var_color,size=:var_size,quantity=:var_quantity,price=:var_price,supplier_id=:var_supplier_id");
			$query->execute(array("var_product_name"=>$product_name,"var_color"=>$color,"var_size"=>$size,"var_price"=>$price,"var_quantity"=>$quantity,"var_supplier_id"=>$supplier_id));
			//---
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$conn->query("delete from products where id=$id");
		}
		public function modelReadCategorySub($category_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from categories where parent_id=$category_id");
			return $query->fetchAll();
		}
		public function modelListCategories(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers order by id desc");
			return $query->fetchAll();
		}
		public function modelGetSupplier($Sup_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers where id=$Sup_id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
	}
 ?>