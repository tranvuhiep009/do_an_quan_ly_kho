<?php 
	trait ModelSuppliers{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from suppliers");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from suppliers where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelUpdate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$supplier_name = $_POST["supplier_name"];	
			$address = $_POST["address"];	
			$phone = $_POST["phone"];	
			$number_tax = $_POST["number_tax"];	
			$supplier_login = $_POST["supplier_login"];	
			$supplier_password = $_POST["supplier_password"];	
			//update name
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("update suppliers set supplier_name=:var_supplier_name,address=:var_address,phone=:var_phone,number_tax=:var_number_tax,supplier_login=:var_supplier_login where id=$id");
			$query->execute(array("var_supplier_name"=>$supplier_name,"var_address"=>$address,"var_phone"=>$phone,"var_number_tax"=>$number_tax,"var_supplier_login"=>$supplier_login));	
			if($supplier_password != ""){
				//ma hoa password 
				$supplier_password = md5($supplier_password);
				$query = $conn->prepare("update suppliers set supplier_password=:var_password where id=$id");
				$query->execute(array("var_password"=>$supplier_password));
			}		
		}
		public function modelCreate(){
			$supplier_name = $_POST["supplier_name"];	
			$address = $_POST["address"];	
			$phone = $_POST["phone"];	
			$number_tax = $_POST["number_tax"];	
			$supplier_login = $_POST["supplier_login"];	
			$supplier_password = $_POST["supplier_password"];	

			$supplier_password = md5($supplier_password);
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert suppliers set supplier_name=:var_supplier_name,address=:var_address,phone=:var_phone,number_tax=:var_number_tax,supplier_login=:var_supplier_login,supplier_password=:var_supplier_password");
			// var_dump($query->execute(array("var_name"=>$name,"var_parent_id"=>$parent_id)));
			$query->execute(array("var_supplier_name"=>$supplier_name,"var_address"=>$address,"var_phone"=>$phone,"var_number_tax"=>$number_tax,"var_supplier_login"=>$supplier_login,"var_supplier_password"=>$supplier_password));
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from suppliers where id=$id");
		}
		public function modelReadSub($category_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from categories where parent_id=$category_id");
			return $query->fetchAll();
		}
		public function modelListCategories(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from categories where parent_id = 0 order by id desc");
			return $query->fetchAll();
		}
		public function getproduct(){
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products ");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
	}
 ?>