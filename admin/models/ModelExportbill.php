<?php 
	trait ModelExportbill{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from exportbill order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from exportbill");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from exportbill where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		
		public function modelCreate(){
			// var_dump($_POST);die();
			$id = isset($_POST["id"])&&$_POST["id"] > 0 ? $_POST["id"] : 0;
			$product_id = $_POST["product_id"];
			$data=$this->modelListproductbyid($product_id);
			foreach($data as $row){
				$price=$row->price;
			};
			$price=$price;
			$create_date = date('Y-m-t');
			$content = $_POST["content"];
			$warehouse_id = $_POST["warehouse_id"];
			$store_id = $_POST["store_id"];
			
			$quantity = $_POST["quantity"];
			// var_dump($quantity);die();
			$user_name = $_SESSION["user_name"];
			// var_dump("content");die()
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert exportbill set id=:var_id,create_date=:var_create_date,content=:var_content,user_name=:var_user_name,warehouse_id=:var_warehouse_id,store_id=:var_store_id");
			$query->execute(array("var_id"=>$id,"var_create_date"=>$create_date,"var_content"=>$content,"var_user_name"=>$user_name,"var_warehouse_id"=>$warehouse_id,"var_store_id"=>$store_id));
            $query = $conn->prepare("insert exportbilldetails set exportbill_id=:var_exportbill_id,product_id=:var_product_id,quantity=:var_quantity,price=:var_price");
            $query->execute(array("var_exportbill_id"=>$id,"var_product_id"=>$product_id,"var_quantity"=>$quantity,"var_price"=>$price));
			//---
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from exportbill where id=$id");
		}
		public function modelGetWarehouse($ware_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse where id=$ware_id");
			return $query->fetch();
		}
		public function modelListWarehouse(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse");
			return $query->fetchAll();
		}
		
		public function modelListStore(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from store");
			return $query->fetchAll();
		}
		public function modelListproduct(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products");
			return $query->fetchAll();
		}
		public function modelListproductbyid($id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products where id=$id");
			return $query->fetchAll();
		}
        public function modelDetail(){
            $id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
            $conn = Connection::getInstance();
            $query = $conn->query("select * from exportbill where id=$id ");
            return $query->fetch();
        }
        public function modelDetailbill(){
            $id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
            $conn = Connection::getInstance();
            $query = $conn->query("select * from exportbilldetails where exportbill_id=$id ");
            return $query->fetch();
        }
        public function modelGetNameStore($id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from store where id=$id");
			return $query->fetch();
		}
		public function modelGetNameProducts($id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products where id=$id");
			return $query->fetch();
		}
		public function showproduct(){
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from products");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
	}
 ?>