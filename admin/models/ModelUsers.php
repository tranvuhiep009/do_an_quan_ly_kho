<?php 
	trait ModelUsers{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from users");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelUpdate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$name = $_POST["user_name"];
			$gender = $_POST["gender"];
			$address = $_POST["address"];
			$phone = $_POST["phone"];
			$position = $_POST["position"];
			$user_login = $_POST["user_login"];
			$password_login = $_POST["password_login"];
			//update name
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("update users set user_name=:var_name,gender=:gender,address=:address,phone=:phone,position=:position where id=$id");
			$query->execute(array("var_name"=>$name,"gender"=>$gender,"address"=>$address,"phone"=>$phone,"position"=>$position));
			//neu password khong rong thi update password
			if($password_login != ""){
				//ma hoa password 
				$password_login = md5($password_login);
				$query = $conn->prepare("update users set password_login=:var_password where id=$id");
				$query->execute(array("var_password"=>$password_login));
			}
			$photo = "";
			if($_FILES["photo"]["name"] != ""){
				//---
				//lay anh cu de xoa
				$oldPhoto = $conn->query("select photo from users where id=$id");
				if($oldPhoto->rowCount() > 0){
					$record = $oldPhoto->fetch();
					//xoa anh
					if($record->photo != "" && file_exists("../assets/upload/user/".$record->photo))
						unlink("../assets/upload/user/".$record->photo);
				}
				//---
				$photo = time()."_".$_FILES["photo"]["name"];
				move_uploaded_file($_FILES["photo"]["tmp_name"],"../assets/upload/user/$photo");
				$query = $conn->prepare("update users set photo=:var_photo where id=$id");
				$query->execute(array("var_photo"=>$photo));
			}
		}
		public function modelCreate(){
			$name = $_POST["user_name"];
			$gender = $_POST["gender"];
			$address = $_POST["address"];
			$phone = $_POST["phone"];
			$position = $_POST["position"];
			$user_login = $_POST["user_login"];
			$password_login = $_POST["password_login"];
			//ma hoa password 
			$password_login = md5($password_login);
			$photo = "";
			if($_FILES["photo"]["name"] != ""){
				$photo = time()."_".$_FILES["photo"]["name"];
				move_uploaded_file($_FILES["photo"]["tmp_name"],"../assets/upload/user/$photo");
			}
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert into users set user_name=:var_name,gender=:gender,address=:address,phone=:phone,photo=:photo,position=:position,user_login=:user_login, password_login=:var_password");
			$query->execute(array("var_name"=>$name,"gender"=>$gender,"address"=>$address,"phone"=>$phone,"photo"=>$photo,"position"=>$position,"user_login"=>$user_login,"var_password"=>$password_login));
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from users where id=$id");
		}
	}
 ?>