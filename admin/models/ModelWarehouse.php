<?php 
	trait ModelWarehouse{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from users");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from warehouse where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelUpdate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$warehouse_name = $_POST["warehouse_name"];
			$address = $_POST["address"];
			$describe = $_POST["content"];
			$user_id = $_POST["user_id"];
			//update name
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("update warehouse set warehouse_name=:var_warehouse_name,address=:var_address,content=:var_describe,user_id=:var_user_id where id=$id");
			$query->execute(array("var_warehouse_name"=>$warehouse_name,"var_address"=>$address,"var_describe"=>$describe,"var_user_id"=>$user_id));
		}
		public function modelCreate(){
			$warehouse_name = $_POST["warehouse_name"];
			$address = $_POST["address"];
			$describe = $_POST["content"];
			$user_id = $_POST["user_id"];
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert warehouse set warehouse_name=:var_warehouse_name,address=:var_address,content=:var_describe,user_id=:var_user_id");
			$query->execute(array("var_warehouse_name"=>$warehouse_name,"var_address"=>$address,"var_describe"=>$describe,"var_user_id"=>$user_id));
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from warehouse where id=$id");
		}
		public function modelListUser(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users order by id desc");
			return $query->fetchAll();
		}
		public function modelGetUser($user_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where id=$user_id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
	}
 ?>