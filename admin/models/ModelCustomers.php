<?php 
	trait ModelCustomers{
		//lay ve danh sach cac ban ghi
		public function modelRead($recordPerPage){
			//lay bien page truyen tu url
			$page = isset($_GET["page"])&&$_GET["page"]>0 ? $_GET["page"]-1 : 0;
			//lay tu ban ghi nao
			$from = $page * $recordPerPage;
			//---
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from store order by id desc limit $from, $recordPerPage");
			//tra ve nhieu ban ghi
			return $query->fetchAll();
			//--- 
		}
		//tinh tong so ban ghi
		public function modelTotalRecord(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select id from store");
			//tra ve so ban ghi
			return $query->rowCount();
		}
		//lay mot ban ghi tuong ung voi id truyen vao
		public function modelGetRecord(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from store where id=$id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelUpdate(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			$store_name = $_POST["store_name"];
			$address = $_POST["address"];
			$phone = $_POST["phone"];
			
			$user_id = $_POST["user_id"];
			//update name
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			$query = $conn->prepare("update store set store_name=:var_store_name,address=:address,user_id=:user_id,phone=:phone where id=$id");
			$query->execute(array("var_store_name"=>$store_name,"user_id"=>$user_id,"address"=>$address,"phone"=>$phone));
		}
		public function modelCreate(){
			$store_name = $_POST["store_name"];
			$address = $_POST["address"];
			$phone = $_POST["phone"];
			$user_id = $_POST["user_id"];
			$conn = Connection::getInstance();
			$query = $conn->prepare("insert into store set store_name=:var_store_names,user_id=:user_id,address=:address,phone=:phone");
			$query->execute(array("var_store_names"=>$store_name,"user_id"=>$user_id,"address"=>$address,"phone"=>$phone));
		}
		public function modelDelete(){
			$id = isset($_GET["id"])&&$_GET["id"] > 0 ? $_GET["id"] : 0;
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$conn->query("delete from store where id=$id");
		}
		public function modelGetUser($user_id){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where id=$user_id");
			//tra ve mot ban ghi
			return $query->fetch();
		}
		public function modelListuser(){
			//lay bien ket noi csdl
			$conn = Connection::getInstance();
			//thuc hien truy van
			$query = $conn->query("select * from users where position=0 order by id desc");
			return $query->fetchAll();
		}
	}
 ?>