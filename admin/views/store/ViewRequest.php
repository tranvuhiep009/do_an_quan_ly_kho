<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
<div class="page-wrapper">
	<div class="col-md-12">
		<div class="nav justify-content-end pt-3 pb-5">
		</div>
		<div class="panel panel-primary">
		    <div class="panel-body">
		        <table class="table table-bordered table-hover">
		            <tr>
		                <th>Tên sản phẩm</th>
		                <th>Số lượng tồn</th>
		                <th style="width:100px;"></th>
		            </tr>
		            <tr>
		                <td>Gối</td>
		                <td>50</td>
		                <td style="text-align:center;">	
		                	<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
							    Chọn
							  </button>
		                </td>
		            </tr>  
		        </table>
		    </div>
		</div>
	</div>

  

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Số lượng yêu cầu xuất </h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Nhập số lượng: <input type="text" name="">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<button class="btn btn-primary">Gửi</button>
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Hủy</button>
      </div>

    </div>
  </div>
</div>
</div>