<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
<div class="col-md-12">
    <div class="nav justify-content-end pt-3 pb-5">
    </div>
    <div style="margin-bottom:5px;">
        <a href="index.php?controller=customers&action=create" class="btn btn-primary">Thêm cửa hàng</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên cửa hàng</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Cửa hàng trưởng</th>
                    <th style="width:100px;"></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->store_name; ?></td>
                    <td><?php echo $rows->address; ?> </td>
                    <td><?php echo $rows->phone; ?></td>
                    <td>
                        <?php 
                            $data = $this->modelGetUser($rows->user_id);
                            echo isset($data->user_name)?$data->user_name:"";
                        ?>
                    </td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=customers&action=update&id=<?php echo $rows->id; ?>"><i class="mdi mdi-lead-pencil h3"></i></a>&nbsp;
                        <a href="index.php?controller=customers&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa cửa  hàng?');"><i class="mdi mdi-delete h3"></i></a>
                    </td>
                </tr>                    
                <?php endforeach; ?>
            </table>
            <style type="text/css">
                .pagination{padding:0px; margin:0px;}
            </style>
            <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">Trang</a></li>
                <?php for($i = 1; $i <= $numPage; $i++): ?>
                <li class="page-item"><a href="index.php?controller=customers&page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a></li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</div>
</div>