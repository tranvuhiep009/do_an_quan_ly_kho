<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>  
 <div class="page-wrapper">                 
<div class="col-md-12">  
    <div class="panel panel-primary">
        <div class="panel-body">
        <form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên người dùng</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->user_name) ? $record->user_name:""; ?>" name="user_name" class="form-control"  required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Giới tính</div>
                <div class="col-md-10">
                    Nam: <input type="radio" name="gender" value="1" checked>
                    Nữ: <input type="radio" name="gender" value="2" >
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Địa chỉ</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->address) ? $record->address:""; ?>" name="address" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Số điện thoại</div>
                <div class="col-md-10">
                    <input type="number" value="<?php echo isset($record->phone) ? $record->phone:""; ?>" name="phone" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Ảnh</div>
                <div class="col-md-10">
                    <input type="file" name="photo">
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Chức vụ</div>
                <div class="col-md-10">
                    <select name="position">
                        <option value="0">Quản lý</option>
                        <option value="1">Thủ kho</option>
                    </select>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên đăng nhập</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->user_login) ? $record->user_login:""; ?>" name="user_login" <?php echo isset($dis)?$dis:'' ?> class="form-control" required>
                </div>
            </div>
            <!-- end rows -->

            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mật khẩu</div>
                <div class="col-md-10">
                    <input type="password" name="password_login" <?php if(isset($record->password_login)): ?> placeholder="Không đổi password thì không nhập thông tin vào ô textbox này" <?php else: ?> required <?php endif; ?> class="form-control">
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <input type="submit" value="Process" class="btn btn-primary">
                </div>
            </div>
            <!-- end rows -->
        </form>
        </div>
    </div>
</div>
</div>