<?php 
	//load file Layout.php
	$this->fileLayout = "Layout.php";
    // var_dump($_SESSION['user_login_tk']);exit();
 ?>
 <h2 class="text-center mt-3" style="font-family:arial">Thống kê</h2>
<!-- Sale & Revenue Start -->
<div class="page-wrapper">
    <div class="container-fluid pt-4 px-4 ">
        <div class="row g-4">
            <div class="d-flex align-items-center justify-content-center">
                <div class="col-3 me-5">
                <a href="index.php?controller=home&action=detailsp">
                    <div class="alert alert-dark rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-chart-area fa-3x text-primary"></i>
                        <div class="ms-3">
                            <p class="mb-2">Số lượng sản phẩm:</p>
                            <h6 class="mb-0"><?php echo $slsp->sl ?> Sản phẩm</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3 me-5">
                <a href="index.php?controller=home&action=detailpn">
                    <div class="alert alert-dark rounded d-flex align-items-center justify-content-between p-4">

                        <i class="fa fa-arrow-circle-down fa-3x text-primary" aria-hidden="true"></i>
                        <div class="ms-3">
                            <p class="mb-2">Thống kê phiếu nhập kho:</p>
                            <h6 class="mb-0"><?php echo $tkpn->slpn ?> Phiếu nhập</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3 me-5">
                <a href="index.php?controller=home&action=detailpx">
                    <div class="alert alert-dark rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-arrow-circle-up fa-3x text-primary" aria-hidden="true"></i>
                        <div class="ms-3">
                            <p class="mb-2">Thống kê phiếu xuất kho:</p>
                            <h6 class="mb-0"><?php echo $tkpx->slpx ?> Sản phẩm</h6>
                        </div>
                    </div>
                </a>
            </div>
            </div>
        </div>
    </div>
</div>
    

            <!-- <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <!-- <div class="row">
                    <!-- Column -->
                   <!--  <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="d-flex flex-wrap align-items-center">
                                            <div>
                                                <h3 class="card-title">Sales Overview</h3>
                                                <h6 class="card-subtitle">Ample Admin Vs Pixel Admin</h6>
                                            </div>
                                            <div class="ms-lg-auto mx-sm-auto mx-lg-0">
                                                <ul class="list-inline d-flex">
                                                    <li class="me-4">
                                                        <h6 class="text-success"><i
                                                                class="fa fa-circle font-10 me-2 "></i>Ample</h6>
                                                    </li>
                                                    <li>
                                                        <h6 class="text-info"><i
                                                                class="fa fa-circle font-10 me-2"></i>Pixel</h6>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="amp-pxl" style="height: 360px;">
                                            <div class="chartist-tooltip"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Our Visitors </h3>
                                <h6 class="card-subtitle">Different Devices Used to Visit</h6>
                                <div id="visitor"
                                    style="height: 290px; width: 100%; max-height: 290px; position: relative;"
                                    class="c3">
                                    <div class="c3-tooltip-container"
                                        style="position: absolute; pointer-events: none; display: none;">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <hr class="mt-0 mb-0">
                            </div>
                            <div class="card-body text-center ">
                                <ul class="list-inline d-flex justify-content-center align-items-center mb-0">
                                    <li class="me-4">
                                        <h6 class="text-info"><i class="fa fa-circle font-10 me-2 "></i>Mobile</h6>
                                    </li>
                                    <li class="me-4">
                                        <h6 class=" text-primary"><i class="fa fa-circle font-10 me-2"></i>Desktop</h6>
                                    </li>
                                    <li class="me-4">
                                        <h6 class=" text-success"><i class="fa fa-circle font-10 me-2"></i>Tablet</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->

           
       