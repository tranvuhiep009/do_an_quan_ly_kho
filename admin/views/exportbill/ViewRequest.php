<?php
//load file Layout.php
$this->fileLayout = "Layout.php";
?>
<div class="page-wrapper">
    <div class="col-md-12">
        <div>
            <h2 class="text-center pt-5">Danh sách yêu cầu</h2>
        </div>
        <div class="nav justify-content-end pt-3 pb-5">
            
        </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                    	Chăn hơi hơi ấm
                    </td>
                    <td>3</td>
                    <td>Chưa xác nhận</td>
                    <td>
                    	<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
					    Xác nhận
					  </button>
					  <button type="button" class="btn btn-danger">Hủy</button>
                    </td>
                </tr>
                <tr>
                    <td>
                        Chăn hơi hơi ấm
                    </td>
                    <td>3</td>
                    <td>Đang xuất hàng</td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Chăn hơi hơi ấm
                    </td>
                    <td>3</td>
                    <td>Xuất thành công</td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Chăn hơi hơi ấm
                    </td>
                    <td>3</td>
                    <td>Đã hủy</td>
                    <td>
                        
                    </td>
                </tr>
            </table>   
            </div>
        </div>
    </div>

    <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Chọn kho xuất </h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Kho: <select style="width: 300px;">
        	<option>Kho 1</option>
        	<option>Kho 2</option>
        	<option>Kho 3</option>
        </select>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<button class="btn btn-primary">Xác nhận</button>
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Không nhập nữa dỗi</button>
      </div>

    </div>
  </div>
</div>
</div>

