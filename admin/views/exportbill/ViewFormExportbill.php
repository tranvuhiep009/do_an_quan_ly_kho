<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>  
 <div class="page-wrapper">                 
<div class="col-md-12">  
    <div class="panel panel-primary">
        <div class="panel-body">
        <form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data"> 
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mã phiếu xuất</div>
                <div class="col-md-2">
                    <input type="number" value="" name="id" class="form-control"  required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mô tả</div>
                <div class="col-md-10">
                    <input type="text" value="" name="content" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Kho xuất</div>
                <div class="col-md-10">
                    <?php
                        $data = $this->modelListWarehouse();
                     ?>
                    <select class="w-25" name="warehouse_id">
                         <?php foreach($data as $value){ ?>
                            <option <?php if(isset($record->warehouse_id)&&$record->warehouse_id==$value->id): ?> selected <?php endif; ?> value="<?php echo $value->id ?>"><?php echo $value->warehouse_name ?></option>
                         <?php } ?>
                     </select>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Cửa hàng nhận</div>
                <div class="col-md-10">
                    <?php
                        $data1 = $this->modelListStore();
                     ?>
                    <select class="w-25" name="store_id">
                         <?php foreach($data1 as $value){ ?>
                            <option <?php if(isset($record->store_id)&&$record->store_id==$value->id): ?> selected <?php endif; ?> value="<?php echo $value->id ?>"><?php echo $value->store_name ?></option>
                         <?php } ?>
                     </select>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Sản phẩm</div>
                <div class="col-md-10">
                    <?php
                        $data2 = $this->modelListproduct();
                     ?>
                    <select class="w-25" name="product_id">
                         <?php foreach($data2 as $value){ ?>
                            <option <?php if(isset($record->product_id)&&$record->product_id==$value->id): ?> selected <?php endif; ?> value="<?php echo $value->id ?>"><?php echo $value->product_name ?></option>
                         <?php } ?>
                     </select>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Số lượng</div>
                <div class="col-md-4">
                    <input type="number" name="quantity" class="form-control" required>
                    <?php isset($_SESSION['error'])?$_SESSION['error']:'' ?>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <input type="submit" value="Lưu" class="btn btn-primary">
                </div>
            </div>
            <!-- end rows -->
        </form>
        </div>
    </div>
</div>
</div>
