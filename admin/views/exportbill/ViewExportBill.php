<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
<div class="page-wrapper">
    <div class="col-md-12">
    	<div class="nav justify-content-end pt-3 pb-5">
	    </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Mã phiếu xuất</th>
                        <th>Ngày tạo</th>
                        <th>Mô tả</th>
                        <th>Người tạo</th>
                        <th>Kho xuất</th>
                        <th>Cửa hàng nhận</th>
                        <th style="width:100px;"></th>
                    </tr>
                    <?php 
                        foreach($data as $rows):
                     ?>
                    <tr>
                        <td><?php echo $rows->id; ?></td>
                        <td><?php echo $rows->create_date; ?></td>
                        <td><?php echo $rows->content; ?></td>
                        <td><?php echo $rows->user_name; ?></td>
                        <td>
                            <?php
                            $data = $this->modelGetWarehouse($rows->warehouse_id);
                            echo isset($data->warehouse_name)?$data->warehouse_name:"";
                            ?>
                        </td>
                        <td>
                            <?php
                                $data = $this->modelGetNameStore($rows->store_id);
                                echo isset($data->store_name)?$data->store_name:"";
                            ?>
                            </td>
                        <td style="text-align:center;">
                            <a href="index.php?controller=exportbill&action=detail&id=<?php echo $rows->id; ?>" ><i class="mdi mdi-information-outline h3"></i></a>
                            <a href="index.php?controller=exportbill&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa hay không?');"><i class="mdi mdi-delete h3"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <style type="text/css">
                    .pagination{padding:0px; margin:0px;}
                </style>
                <ul class="pagination">
                    <li class="page-item"><a href="#" class="page-link">Trang</a></li>
                    <?php for($i = 1; $i <= $numPage; $i++): ?>
                    <li class="page-item"><a href="index.php?controller=exportbill&page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a></li>
                    <?php endfor; ?>
                </ul>
            </div>
        </div>
    </div>
</div>