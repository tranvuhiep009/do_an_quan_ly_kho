<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng tồn</th>
                    <th>Đơn giá</th>
                    <th>Kho</th>
                    <th></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->product_name; ?></td>
                    <td><?php echo $rows->quantity; ?></td>
                    <td><?php echo $rows->price; ?></td>
                    <td>Kho 1</td>
                    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
                                Yêu cầu xuất
                              </button></td>
                </tr>                    
                <?php endforeach; ?>
                <tr>
                    <td>Chăn nhung siêu ấm</td>
                    <td>13</td>
                    <td>4200000</td>
                    <td>Kho 2</td>
                    <td><button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#myModal">
                                Hủy
                              </button><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#myModal">
                                Đã nhận
                              </button></td>
                </tr>   
            </table>
        </div>
    </div>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Số lượng yêu cầu xuất </h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Nhập số lượng: <input type="text" name="">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-primary">Gửi</button>
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Hủy</button>
      </div>

    </div>
  </div>
</div>
</div>