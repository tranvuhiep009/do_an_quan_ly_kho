<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
<div class="page-wrapper">
    <div class="col-md-12">
        <div>
            <div class="nav justify-content-end pt-3 pb-5">
                <input class="se" style="width: 400px;border-radius: 10px 0px 0px 10px;" id="key" type="text" name="" placeholder="Nhập tên kho">
                <input class="bt" type="button" value="Tìm" onclick="location.href = 'index.php?controller=search&action=searchkho&keysearch=' + document.getElementById('key').value;" name="">
            </div>
        <div class="smart-search">
        </div>
        <div style="margin-bottom:5px;">
            <a href="index.php?controller=warehouse&action=create" class="btn btn-primary">Thêm kho</a>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Tên kho</th>
                        <th>Địa chỉ</th>
                        <th>Mô tả</th>
                        <th>Tên thủ kho</th>
                        <th style="width:100px;"></th>
                    </tr>
                    <?php 
                        foreach($data as $rows):
                     ?>
                    <tr>
                        <td><?php echo $rows->warehouse_name; ?></td>
                        <td><?php echo $rows->address; ?></td>
                        <td><?php echo $rows->content; ?></td>
                        <td>
                            <?php 
                                $data = $this->modelGetUser($rows->user_id);
                                echo isset($data->user_name)?$data->user_name:"";
                            ?>
                        </td>
                        <td style="text-align:center;">
                            <a href="index.php?controller=warehouse&action=update&id=<?php echo $rows->id; ?>">Sửa</a>&nbsp;
                            <a href="index.php?controller=warehouse&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa hay không?');">Xóa</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>