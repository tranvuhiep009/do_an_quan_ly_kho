<?php 
	//load file Layout.php
	$this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
  	<h2 class="text-center pt-5">Thống kê phiếu nhập kho</h2>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Thời gian</th>
      <th scope="col">Số phiếu</th>
    </tr>
  </thead>
  <tbody>
  	<?php 
  		foreach($theothang as $key=>$rows){
  	 ?>
    <tr>
      <td><?php echo 'Tháng '.$key+1 ?></td>
      <td><?php echo $rows ?></td>
    </tr>
<?php } ?>
  </tbody>
</table>
</div>