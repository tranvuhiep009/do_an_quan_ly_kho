<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
<div class="col-md-12">
    <div class="nav justify-content-end pt-3 pb-5">
        <input class="se" style="width: 400px;border-radius: 10px 0px 0px 10px;" id="key" type="text" name="" placeholder="Nhập tên sản phẩm">
        <input class="bt" type="button" value="Tìm" onclick="location.href = 'index.php?controller=search&action=searchKey_sp&keysearch=' + document.getElementById('key').value;" name="">
    </div>
    <div style="margin-bottom:5px;">
        <a href="index.php?controller=products&action=create" class="btn btn-primary">Thêm sản phẩm</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Màu</th>
                    <th>Cỡ</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    <th>Nhà cung cấp</th>
                    <th>Kho</th>
                    <th style="width:100px;"></th>
                    <th></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->product_name; ?></td>
                    <td><?php echo $rows->color; ?> </td>
                    <td><?php echo $rows->size; ?></td>
                    <td><?php echo $rows->quantity; ?></td>
                    <td><?php echo $rows->price; ?></td>
                    <td style="text-align: center;">
                        <?php 
                            $data = $this->modelGetSupplier($rows->supplier_id);
                            echo isset($data->supplier_name)?$data->supplier_name:"";
                        ?>
                    </td>
                    <td>Kho 1</td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=products&action=update&id=<?php echo $rows->id; ?>"><i class="mdi mdi-lead-pencil h3"></i></a>&nbsp;
                        <a href="index.php?controller=products&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa sản phẩm?');"><i class="mdi mdi-delete h3"></i></a>
                    </td>
                    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
                                Yêu cầu nhập
                              </button></td>
                </tr>                    
                <?php endforeach; ?>
                    <td>Gối ôm</td>
                    <td>Đen </td>
                    <td>10x20</td>
                    <td>17</td>
                    <td>200000</td>
                    <td style="text-align: center;">
                        Hiep
                    </td>
                    <td>Kho 2</td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=products&action=update&id=<?php echo $rows->id; ?>"><i class="mdi mdi-lead-pencil h3"></i></a>&nbsp;
                        <a href="index.php?controller=products&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa sản phẩm?');"><i class="mdi mdi-delete h3"></i></a>
                    </td>
                    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
                                Yêu cầu nhập
                              </button></td>
            </table>
            <style type="text/css">
                .pagination{padding:0px; margin:0px;}
            </style>
            <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">Trang</a></li>
                <?php for($i = 1; $i <= $numPage; $i++): ?>
                <li class="page-item"><a href="index.php?controller=products&page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a></li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Số lượng yêu nhập </h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Nhập số lượng: <input type="text" name="">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-primary">Gửi</button>
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Không nhập nữa dỗi</button>
      </div>

    </div>
  </div>
</div>
</div>