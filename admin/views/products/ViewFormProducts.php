<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
<div class="page-wrapper">
<div class="col-md-12">  
    <div class="panel panel-primary">
        <div class="panel-heading"></div>
        <div class="panel-body">
        <!-- muon upload file thi trong the form phai co thuoc tinh sau: enctype="multipart/form-data" -->
        <form method="post" enctype="multipart/form-data" action="<?php echo $action; ?>">
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên sản phẩm</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->product_name)?$record->product_name:""; ?>" name="product_name" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Màu</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->color)?$record->color:""; ?>" name="color" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Cỡ</div>
                <div class="col-md-10" >
                    <input type="text" value="<?php echo isset($record->size)?$record->size:""; ?>" name="size" class="form-control w-25" required>
                </div>
            </div>
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Số lượng</div>
                <div class="col-md-10">
                    <input type="number" value="<?php echo isset($record->quantity)?$record->quantity:""; ?>" name="quantity" class="form-control w-25" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Giá</div>
                <div class="col-md-10" >
                    <input type="number" value="<?php echo isset($record->price)?$record->price:""; ?>" name="price" class="form-control w-25" required>
                </div>
            </div>
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Nhà cung cấp </div>
                <div class="col-md-10" >
                    <?php
                        $data = $this->modelListCategories();
                     ?>
                     <select class="w-25" name="supplier_id">
                         <?php foreach($data as $value){ ?>
                            <option <?php if(isset($record->supplier_id)&&$record->supplier_id==$value->id): ?> selected <?php endif; ?> value="<?php echo $value->id ?>"><?php echo $value->supplier_name ?></option>
                         <?php } ?>
                     </select>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <input type="submit" value="Thực hiện" class="btn btn-primary">
                </div>
            </div>
            <!-- end rows -->
        </form>
        </div>
    </div>
</div>
</div>