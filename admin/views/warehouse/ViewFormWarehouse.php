<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>  
 <div class="page-wrapper">                 
<div class="col-md-12">  
    <div class="panel panel-primary">
        <div class="panel-body">
        <form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên kho</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->warehouse_name) ? $record->warehouse_name:""; ?>" name="warehouse_name" class="form-control"  required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Địa chỉ</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->address) ? $record->address:""; ?>" name="address" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mô tả</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->content) ? $record->content:""; ?>" name="content" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Chủ</div>
                <div class="col-md-10" >
                    <?php
                        $data = $this->modelListUser();
                     ?>
                     <select class="w-25" name="user_id">
                         <?php foreach($data as $value){ ?>
                            <option <?php if(isset($record->user_id)&&$record->user_id==$value->id): ?> selected <?php endif; ?> value="<?php echo $value->id ?>"><?php echo $value->user_name ?></option>
                         <?php } ?>
                     </select>
                </div>
            </div>
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <input type="submit" value="Process" class="btn btn-primary">
                </div>
            </div>
            <!-- end rows -->
        </form>
        </div>
    </div>
</div>
</div>