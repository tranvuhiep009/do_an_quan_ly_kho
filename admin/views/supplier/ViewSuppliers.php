<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
 <div class="page-wrapper">
<div class="col-md-12">
    <div class="nav justify-content-end pt-3 pb-5">
        <input class="se" style="width: 400px;border-radius: 10px 0px 0px 10px;" id="key" type="text" name="" placeholder="Nhập tên nhà cung cấp">
        <input class="bt" type="button" value="Tìm" onclick="location.href = 'index.php?controller=search&action=searchKey_ncc&keysearch=' + document.getElementById('key').value;" name="">
    </div>
    <div style="margin-bottom:5px;">
        <a href="index.php?controller=suppliers&action=create" class="btn btn-primary">Thêm nhà cung cấp</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên nhà cung cấp</th>
                    <th>Địa chỉ nhà cung cấp</th>
                    <th>Số điện thoại</th>
                    <th>Mã số thuế</th>
                    <th>Tên đăng nhập</th>
                    <th style="width:100px;"></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->supplier_name; ?></td>
                    <td><?php echo $rows->address; ?></td>
                    <td><?php echo $rows->phone; ?></td>
                    <td><?php echo $rows->number_tax; ?></td>
                    <td><?php echo $rows->supplier_login; ?></td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=suppliers&action=update&id=<?php echo $rows->id; ?>"><i class="mdi mdi-lead-pencil h3"></i></a>&nbsp;
                        <a href="index.php?controller=suppliers&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có chắc muốn xóa không?');"><i class="mdi mdi-delete h3"></i></a>
                    </td>
                </tr>
            <?php endforeach ?>
            </table>
            <style type="text/css">
                .pagination{padding:0px; margin:0px;}
            </style>
            <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">Trang</a></li>
                <?php for($i = 1; $i <= $numPage; $i++): ?>
                <li class="page-item"><a href="index.php?controller=suppliers&page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a></li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</div>
</div>