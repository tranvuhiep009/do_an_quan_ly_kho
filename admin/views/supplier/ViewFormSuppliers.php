<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>          
  <div class="page-wrapper">            
<div class="col-md-12">  
    <div class="panel panel-primary">
        <div class="panel-body">
        <form method="post" action="<?php echo $action; ?>">
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên nhà cung cấp</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->supplier_name)?$record->supplier_name:""; ?>" name="supplier_name" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Địa chỉ nhà cung cấp</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->address)?$record->address:""; ?>" name="address" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Số điện thoại</div>
                <div class="col-md-10">
                    <input type="number" value="<?php echo isset($record->phone)?$record->phone:""; ?>" name="phone" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mã thuế</div>
                <div class="col-md-10">
                    <input type="number" value="<?php echo isset($record->number_tax)?$record->number_tax:""; ?>" name="number_tax" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Tên đăng nhập</div>
                <div class="col-md-10">
                    <input type="text" value="<?php echo isset($record->supplier_login)?$record->supplier_login:""; ?>" name="supplier_login" class="form-control" required>
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2">Mật khẩu</div>
                <div class="col-md-10">
                    <input type="password" name="supplier_password" <?php if(isset($record->supplier_password)): ?> placeholder="Không đổi password thì không nhập thông tin vào ô textbox này" <?php else: ?> required <?php endif; ?> class="form-control">
                </div>
            </div>
            <!-- end rows -->
            <!-- rows -->
            <div class="row" style="margin-top:5px;">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <input type="submit" value="Thực hiện" class="btn btn-primary">
                </div>
            </div>
            <!-- end rows -->
        </form>
        </div>
    </div>
</div>
</div>