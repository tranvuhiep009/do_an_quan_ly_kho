<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
<div class="col-md-12">
    <div class="nav justify-content-end pt-3 pb-5">
        <input class="se" style="width: 400px;border-radius: 10px 0px 0px 10px;" id="key" type="text" name="" placeholder="Nhập tên khách hàng">
        <input class="bt" type="button" value="Tìm" onclick="location.href = 'index.php?controller=search&action=searchKey_khach&keysearch=' + document.getElementById('key').value;" name="">
    </div>
    <div style="margin-bottom:5px;">
        <a href="index.php?controller=customers&action=create" class="btn btn-primary">Thêm khách hàng</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên khách</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Giới tính</th>
                    <th style="width:100px;"></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->customer_name; ?></td>
                    <td><?php echo $rows->address; ?> </td>
                    <td><?php echo $rows->phone; ?></td>
                    <td>
                        <?php if ($rows->gender==1) {
                            echo 'Nam';
                        }else{echo 'Nữ';} ?>
                    </td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=customers&action=update&id=<?php echo $rows->id; ?>">Sửa</a>&nbsp;
                        <a href="index.php?controller=customers&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa khách hàng?');">Xóa</a>
                    </td>
                </tr>                    
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
</div>