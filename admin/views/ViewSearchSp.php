<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
<div class="col-md-12">
    <div class="nav justify-content-end pt-3 pb-5">
        <input class="se" style="width: 400px;border-radius: 10px 0px 0px 10px;" id="key" type="text" name="" placeholder="Nhập tên sản phẩm">
        <input class="bt" type="button" value="Tìm" onclick="location.href = 'index.php?controller=search&action=searchKey_sp&keysearch=' + document.getElementById('key').value;" name="">
    </div>
    <div style="margin-bottom:5px;">
        <a href="index.php?controller=products&action=create" class="btn btn-primary">Thêm sản phẩm</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Màu</th>
                    <th>Cỡ</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    <th>Nhà cung cấp</th>
                    <th style="width:100px;"></th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->product_name; ?></td>
                    <td><?php echo $rows->color; ?> </td>
                    <td><?php echo $rows->size; ?></td>
                    <td><?php echo $rows->quantity; ?></td>
                    <td><?php echo $rows->price; ?></td>
                    <td style="text-align: center;">
                        <?php 
                            $data = $this->modelGetSupplier($rows->supplier_id);
                            echo isset($data->supplier_name)?$data->supplier_name:"";
                        ?>
                    </td>
                    <td style="text-align:center;">
                        <a href="index.php?controller=products&action=update&id=<?php echo $rows->id; ?>">Sửa</a>&nbsp;
                        <a href="index.php?controller=products&action=delete&id=<?php echo $rows->id; ?>" onclick="return window.confirm('Bạn có muốn xóa sản phẩm?');">Xóa</a>
                    </td>
                </tr>                    
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
</div>