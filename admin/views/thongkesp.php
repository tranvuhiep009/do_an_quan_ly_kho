<?php 
    //load file Layout.php
    $this->fileLayout = "Layout.php";
 ?>
  <div class="page-wrapper">
    <h2 class="text-center pt-3 mb-3" style="font-family:arial">Thống kê số lượng hàng tồn</h2>
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng tồn kho</th>
                </tr>
                <?php 
                    foreach($data as $rows):
                 ?>
                <tr>
                    <td><?php echo $rows->product_name; ?></td>
                    <td><?php echo $rows->quantity; ?></td>
                </tr>                    
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
</div>