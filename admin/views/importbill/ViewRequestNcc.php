<?php
//load file Layout.php
$this->fileLayout = "Layout.php";
?>
<div class="page-wrapper">
    <div class="col-md-12">
        <div>
            <h2 class="text-center pt-5">Danh sách yêu cầu</h2>
        </div>
        <div class="nav justify-content-end pt-3 pb-5">
            
        </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Nhà cung cấp</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
                
                <tr>
                     <td>
                        Đệm lò xo
                    </td>
                    <td>25</td>
                    <td>Masan</td>
                    <td>Chờ xác nhận</td>
                    <td><button class="btn btn-success">Xác nhận</button><button class="btn btn-danger">Hủy</button></td>
                </tr>
                <tr>
                     <td>
                        Chăn lông cừu
                    </td>
                    <td>5</td>
                    <td>Everon</td>
                    <td>Đã xác nhận</td>
                    <td></td>
                </tr>
                <tr>
                     <td>
                        Gối matxa
                    </td>
                    <td>10</td>
                    <td>Everon</td>
                    <td>Đã hủy</td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        Bộ chăn ga khủng long
                    </td>
                    <td>10</td>
                    <td>Havico</td>
                    <td>Chờ xác nhận</td>
                    <td><button class="btn btn-success">Xác nhận</button><button class="btn btn-danger">Hủy</button></td>
                </tr>
            </table>   
            </div>
        </div>
    </div>

</div>

