<?php
//load file Layout.php
$this->fileLayout = "Layout.php";
?>
<div class="page-wrapper">
    <div class="col-md-12">
        <div>
            <h2 class="text-center pt-5">Chi tiết phiếu nhập kho</h2>
        </div>
        <div class="nav justify-content-end pt-3 pb-5">
            
        </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table">
                <tr>
                    <th style="width: 100px;">Mã phiếu nhập</th>
                    <td><?php echo $data->id; ?></td>
                </tr>
                <tr>
                    <th style="width: 100px;">Ngày tạo</th>
                    <td><?php echo $data->create_date; ?></td>
                </tr>
               <!-- <tr>
                    <th style="width: 100px;">Mô tả</th>
                    <td><?php /*echo $data->content; */?></td>
                </tr>
                <tr>
                    <th style="width: 100px;">Tên kho</th>
                    <td>
                        <?php
/*                        $dataa = $this->modelGetWarehouse($data->warehouse_id);
                        $details = $this->modelDetailbill();
                        echo $dataa->warehouse_name;
                        */?>
                    </td>
                </tr>-->
                <tr>
                    <th style="width: 100px;">Tên nhà cung cấp</th>
                    <td><?php echo $data->user_name; ?></td>
                </tr>
            </table>

            <table class="table table-bordered table-hover">
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    
                </tr>
                <tr>
                    <td><?php echo $details->product_name; ?></td>
                    <td><?php echo $details->quantity ?></td>
                    <td><?php echo $details->price; ?></td>
                </tr>
            </table>   
            </div>
        </div>
    </div>
</div>

